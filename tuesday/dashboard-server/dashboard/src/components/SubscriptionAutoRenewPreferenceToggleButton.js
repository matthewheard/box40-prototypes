import React, { Component } from 'react';
import '../App.css';

class SubscriptionAutoRenewPreferenceToggleButton extends Component {
  constructor(props) {
    super(props);

    this.click = this.click.bind(this);
  }

  getRequestConfiguration() {
    return {
      method: 'POST',
      headers: new Headers(),
      mode: 'cors',
      cache: 'default'
    };
  }

  getRequest() {
    const uri = "//localhost:3000/toggleSubscriptionAutoRenewPreference";
    const config = this.getRequestConfiguration();
    return new Request(uri, config);
  }

  click() {
    const request = this.getRequest();
    fetch(request);
  }

  render() {
    return (<button onClick={this.click}>Toggle auto renewal preference</button>);
  }
}

export default SubscriptionAutoRenewPreferenceToggleButton;
