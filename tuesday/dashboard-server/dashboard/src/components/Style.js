import React, { Component } from 'react';
import '../App.css';

class Style extends Component {
  constructor(props) {
    super(props);

    this.state = {
      contents: ""
    };

    this.extractJsonFromResponse = this.extractJsonFromResponse.bind(this);
    this.presentStyleData = this.presentStyleData.bind(this);
  }

  getRequestConfiguration() {
    return {
      method: 'GET',
      headers: new Headers(),
      mode: 'cors',
      cache: 'default'
    };
  }

  getRequest() {
    const uri = "//localhost:3000/style.json";
    const config = this.getRequestConfiguration();
    return new Request(uri, config);
  }

  extractJsonFromResponse(response) {
    return response.json();
  }

  presentStyleData(styleData) {
    console.log(styleData);

    this.setState(
      prevState => (
        {
          contents: styleData['contents']
        }
      )
    );
  }

  componentDidMount() {
    const request = this.getRequest();

    fetch(request)
      .then(this.extractJsonFromResponse)
      .then(this.presentStyleData);
  }

  render() {
    return (
      <div className="Style">
        {this.state.contents}
      </div>
    );
  }
}

export default Style;
