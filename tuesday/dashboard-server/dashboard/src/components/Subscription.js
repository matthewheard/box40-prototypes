import React, { Component } from 'react';
import '../App.css';

import SubscriptionAutoRenewPreferenceToggleButton from './SubscriptionAutoRenewPreferenceToggleButton.js';

class Subscription extends Component {
  constructor(props) {
    super(props);

    this.state = {
      endDate: "",
      autoRenewPreference: true
    };

    this.extractJsonFromResponse = this.extractJsonFromResponse.bind(this);
    this.presentSubscriptionData = this.presentSubscriptionData.bind(this);
  }

  getRequestConfiguration() {
    return {
      method: 'GET',
      headers: new Headers(),
      mode: 'cors',
      cache: 'default'
    };
  }

  getRequest() {
    const uri = "//localhost:3000/subscription.json";
    const config = this.getRequestConfiguration();
    return new Request(uri, config);
  }

  extractJsonFromResponse(response) {
    return response.json();
  }

  presentSubscriptionData(styleData) {
    console.log(styleData);

    this.setState(
      prevState => (
        {
          endDate: styleData['endDate'],
          autoRenewPreference: styleData['autoRenewPreference']
        }
      )
    );
  }

  componentDidMount() {
    const request = this.getRequest();

    fetch(request)
      .then(this.extractJsonFromResponse)
      .then(this.presentSubscriptionData);
  }

  render() {
    console.log(this.state.autoRenewPreference);
    return (
      <div className="Subscription">
        <div>{this.state.endDate}</div>
        <div>{this.state.autoRenewPreference ? 'true' : 'false'}</div>
        <SubscriptionAutoRenewPreferenceToggleButton />
      </div>
    );
  }
}

export default Subscription;
