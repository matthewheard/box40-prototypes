import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import Style from './components/Style.js';
import Subscription from './components/Subscription.js';

class App extends Component {
  render() {
    return (
      <div className="App">
        <ul>
          <li>
            <h3>My style</h3>
            <Style />
          </li>
          <li>
            <h3>My photos</h3>
            <div className="Photos"></div>
          </li>
          <li>
            <h3>My sizes</h3>
            <div className="Sizes"></div>
          </li>
          <li>
            <h3>My budget</h3>
            <div className="Budget"></div>
          </li>
          <li>
            <h3>My orders and boxplan</h3>
            <div className="Orders"></div>
          </li>
          <li>
            <h3>My boxpoints and referrals</h3>
            <div className="Boxpoints"></div>
          </li>
          <li>
            <h3>My account</h3>
            <div className="Account"></div>
          </li>
          <li>
            <h3>My subscription</h3>
            <Subscription />
          </li>
          <li>
            <h3>My payment</h3>
            <div className="Payment"></div>
          </li>
        </ul>
      </div>
    );
  }
}

export default App;
