Rails.application.routes.draw do
  get 'style', to: 'dashboard_components#style'
  get 'subscription', to: 'dashboard_components#subscription'
  post 'toggleSubscriptionAutoRenewPreference',
    to: 'dashboard_components#toggleSubscriptionAutoRenewPreference'
end
