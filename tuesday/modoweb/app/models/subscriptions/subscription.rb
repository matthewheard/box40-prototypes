module Subscriptions
  class Subscription
    attr_reader :auto_renew_preference

    def initialize
      @auto_renew_preference = true
    end

    def toggle_auto_renew_preference
      @auto_renew_preference = !@auto_renew_preference
    end
  end
end
