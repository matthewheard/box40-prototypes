module Subscriptions
  class ToggleSubscriptionAutoRenewPreference
    def initialize(customer_repository, customer_id)
      @customer_repository = customer_repository
      @customer_id = customer_id
    end

    def call
      subscription.toggle_auto_renew_preference

      subscription.auto_renew_preference
    end

    private

    attr_reader :customer_id, :customer_repository

    def subscription
      @subscription ||= customer.subscription
    end

    def customer
      @customer ||= customer_repository.find(customer_id)
    end
  end
end
