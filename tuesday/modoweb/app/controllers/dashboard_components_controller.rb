class DashboardComponentsController < ApplicationController
  def style
    render json: { contents: 'Casual' }
  end

  def subscription
    render json: {
      contents: '1 Dec 2018',
      endDate: '1 Dec 2018',
      autoRenewPreference: true
    }
  end

  class Customer
    def initialize(id, subscription_repository)
      @id = id
      @subscription_repository = subscription_repository
    end

    def subscription
      @subscription ||= subscription_repository.find_by_customer_id(id)
    end

    private

    attr_reader :id, :subscription_repository
  end

  class CustomerRepository
    def find(id)
      subscription_repository = Subscriptions::SubscriptionRepository.new
      Customer.new(id, subscription_repository)
    end
  end

  def toggleSubscriptionAutoRenewPreference
    customer_id = 1 # from params
    toggler_class = Subscriptions::ToggleSubscriptionAutoRenewPreference
    customer_repository = CustomerRepository.new
    toggler = toggler_class.new(customer_repository, customer_id)
    new_preference = toggler.call

    render json: { newPreference: new_preference }
  end
end
