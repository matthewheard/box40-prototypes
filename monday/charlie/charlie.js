'use strict';

const e = React.createElement;

function Charlie(props) {
  const children = [
    e('h1', [], 'Hello Charlie'),
    e('a', {"href": "/next-for-charlie"}, 'Next')
  ];
  return e('div', [], children);
}

const domContainer = document.querySelector('#root');
ReactDOM.render(e(Charlie), domContainer);
