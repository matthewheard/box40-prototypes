Rails.application.routes.draw do
  get '/bravo', to: 'react_apps#bravo'
  get '/charlie', to: 'react_apps#charlie'
end
