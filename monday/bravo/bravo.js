'use strict';

const e = React.createElement;

function Bravo(props) {
  return e('div', [], 'Hello Bravo');
}

const domContainer = document.querySelector('#root');
ReactDOM.render(e(Bravo), domContainer);
